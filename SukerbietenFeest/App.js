import React from 'react';
import ReactDOM from 'react-dom';
import { StyleSheet, Text, View, Button, Alert, AppRegistry, TextInput, Platform, TouchableOpacity  } from 'react-native';

export default class App extends React.Component {
constructor(props){
  super(props);
  this.state = {name: ''};

  this.handleSubmit = this.handleSubmit.bind(this);
}

  handleSubmit() {
    Alert.alert(this.state.name)

  }

  render() {
    return (
      <View style={styles.container}>

        <View style={styles.header}>
        <Text style={styles.headerText}>SUKERBIETENFEEST</Text>
        </View>

        <View style={styles.picture}>
          <Text style={{textAlignVertical: 'center'}}>Hier komt een fotootje van eventuele evenementen, sponsoren en datums</Text>
        </View>
        <Text>
        Naam:
        </Text>
          <TextInput
          style={styles.input}
          value={this.state.name}
          onChangeText={ (name) => this.setState({name})}
          clearTextOnFocus={true}
          clearButtonMode={'unless-editing'}
           />
        <TouchableOpacity>
        <Button
        buttonStyle={styles.button}
        onPress={this.handleSubmit}
        title="Submit"
        color='black'
        backgroundColor='orange'
        accessibilityLabel="Learn more about this button"
        raised={true}
        />
        </TouchableOpacity>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  ...Platform.select({
    ios: {

      container: {
        flex: 1,
        backgroundColor: '#fff',
      },
      button: {
        backgroundColor: 'skyblue',
        borderRadius: 15
      },
      input: {
        width: 150,
        height: 25,
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2
      },
      header: {
        alignSelf:'stretch',
        backgroundColor:'#ea4f00',
        alignItems:'center'
      },
      headerText: {
        marginTop: 25,
        marginBottom: 10,
        fontSize: 30,
        color: '#fff',
      },
      picture: {
        height: 150,
        alignSelf: 'stretch',
        alignItems:'center',
        alignContent: 'center',
        backgroundColor: 'lightgreen',
      }

    },
    android: {
      container: {
        flex: 1,
        backgroundColor: 'skyblue',
      },
      button: {
        backgroundColor: 'skyblue',
        borderRadius: 15
      },
      input: {
        width: 150,
        height: 25,
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 2
      },
      header: {
        alignSelf:'stretch',
        backgroundColor:'#ea4f00',
        alignItems:'center'
      },
      headerText: {
        marginTop: 25,
        marginBottom: 10,
        fontSize: 30,
        color: '#fff',
      },
      picture: {
        height: 150,
        alignSelf: 'stretch',
        alignItems:'center',
        alignContent: 'center',
        backgroundColor: 'lightgreen',
      }
    }
  }),
});
